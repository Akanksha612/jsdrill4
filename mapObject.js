

function map(obj,cb)
{

  if(obj==undefined ||cb==undefined)
  return "undefined";

  if(obj==null)
  return {};

  for(var prop in obj)
  {
     obj[prop]= cb(obj[prop]);
  }

 return obj;
}

module.exports.map=map;