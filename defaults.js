
function defaults(obj, defaultProps)
{

if(obj==undefined && defaultProps==undefined)
return undefined;

if(obj==null && defaultProps==null)
return {};

if(defaultProps==null)
return obj;

if(obj==null)
return defaultProps;



for(var prop in defaultProps)
{

    if(!obj.hasOwnProperty(prop))
    obj[prop]=defaultProps[prop];
    

}

return obj;

}


module.exports.defaults=defaults;